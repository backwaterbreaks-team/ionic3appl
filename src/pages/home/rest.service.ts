import { Injectable } from '@angular/core';
import { Http,Response } from '@angular/http';
import 'rxjs/Rx';
@Injectable()
export class RestService{
constructor(private http : Http){}
// https://private-035701-bbinternshipapi.apiary-mock.com/bookings
getBookings(){
    return this.http.get(' http://localhost:3000/bookings')
    .map(
        (response: Response)=>{
                const data = response.json();
                 return data;
        }
    )
    ;
}

}