import { Component } from '@angular/core';
import { ToastController,ToastOptions ,NavController } from 'ionic-angular';
import { RestService } from "./rest.service";
@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  bookings = [];
toastOptions : ToastOptions
  constructor(private toast: ToastController,public navCtrl: NavController,private restService: RestService) {
this.toastOptions ={
  message :'Item was cancelled',
  duration:3000
}
  }
display(){
 this.toast.create(this.toastOptions).present();
}
onGet(){
  this.restService.getBookings().
  subscribe(
     (booking : any[]) => {
       this.bookings = booking
       
     },
      (error) => console.log(error)
  );

}


}
